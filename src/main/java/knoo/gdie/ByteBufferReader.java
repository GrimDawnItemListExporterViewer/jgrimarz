package knoo.gdie;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by knoodrake on 26/04/2017.
 */
public class ByteBufferReader {

    private final byte[] buffer;
    private int ptr = 0;

    public ByteBufferReader(byte[] buffer) {
        this.buffer = buffer;
    }

    public boolean readBool() {
        for(int i=0; i<2; i++) {
            if(buffer[ptr + i] == 1) {
                ptr += 4;
                return true;
            }
        }
        ptr += 4;
        return false;
    }

    public short readUShort() {
        byte[] shortBuff = new byte[4];
        for(int i=0; i<2; i++) {
            shortBuff[i] = buffer[ptr + i];
        }
        for(int i=2; i<4; i++) {
            shortBuff[i] = 0;
        }
        int aShort = ByteBuffer.wrap(shortBuff).order(ByteOrder.LITTLE_ENDIAN).getInt() & 0xFF;
        ptr += 2;
        return (short) aShort;
    }

    public int readUInt() {
        byte[] intBuff = new byte[8];
        for(int i=0; i<4; i++) {
            intBuff[i] = buffer[ptr + i];
        }
        for(int i=4; i<8; i++) {
            intBuff[i] = 0;
        }
        long anInt = ByteBuffer.wrap(intBuff).order(ByteOrder.LITTLE_ENDIAN).getLong() & 0xFF;
        ptr += 4;
        return (int) anInt;
    }


    public int readUIntplop() {
        byte[] intBuff = new byte[8];
        for(int i=0; i<4; i++) {
            intBuff[7 - i] = buffer[ptr + i];
        }
        for(int i=0; i<4; i++) {
            intBuff[i] = 0;
        }
        long anInt = ByteBuffer.wrap(intBuff).getLong() & 0xFFFFFFFF;
        ptr += 4;
        return (int) anInt;
    }

    public int readInt() {
        byte[] longBuff = new byte[4];
        for(int i=0; i<4; i++) {
            longBuff[i] = buffer[ptr + i];
        }
        ptr += 4;
        return ByteBuffer.wrap(longBuff).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    public float readFloat() {
        byte[] longBuff = new byte[4];
        for(int i=0; i<4; i++) {
            longBuff[i] = buffer[ptr + i];
        }
        ptr += 4;
        return ByteBuffer.wrap(longBuff).order(ByteOrder.LITTLE_ENDIAN).getFloat();
    }

    public boolean canRead() {
        return ptr < buffer.length;
    }
}
