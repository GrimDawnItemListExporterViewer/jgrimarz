package knoo.gdie;

import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4SafeDecompressor;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ARZRecord  {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "  ";

    public String recordFile;

    public int recordFileIndex;
    public int recordTypeLength;
    public String recordType;
    public int recordDataOffset;
    public int recordDataSizeCompressed;
    public int recordDataSizeDecompressed;
    public byte[] recordData = new byte[8];

    public Map<String, String> getRecordFileData() {
        return recordFileData;
    }

    Map<String, String> recordFileData;

    public void read() {
        recordFileIndex = LERABuffer.get().readInt();
        recordTypeLength = LERABuffer.get().readInt();
        recordType = LERABuffer.get().readString(recordTypeLength);
        recordDataOffset = LERABuffer.get().readInt();
        recordDataSizeCompressed = LERABuffer.get().readInt();
        recordDataSizeDecompressed = LERABuffer.get().readInt();
        recordData = LERABuffer.get().readBytes(8);
    }

    public void decompress(List<String> keptValues) {
        ByteBufferReader dataBuff = getDecompressedByteBufferReader();
        recordFileData = new HashMap<>();

        while (dataBuff.canRead())
        {

            short dataType = dataBuff.readUShort();
            short dataCount = dataBuff.readUShort();
            int dataString = dataBuff.readUIntplop();

            List<String> values = new ArrayList<>();
            for(short i=0; i<dataCount; i++) {
                switch (dataType) {
                    case 0:
                        values.add(String.valueOf(dataBuff.readUInt()));
                        break;
                    case 1:
                        values.add(String.valueOf(dataBuff.readFloat()));
                        break;
                    case 2:
                        int stringIndex = dataBuff.readInt();
                        values.add(ARZDatabase.get().getString(stringIndex));
                        break;
                    case 3:
                        values.add(String.valueOf(dataBuff.readBool()));
                        break;
                    default:
                        values.add("TODO : UNKNOWN Type");
                        break;
                }
            }
            String entryName = ARZDatabase.get().getString(dataString);
            if(keptValues.isEmpty() || keptValues.contains(entryName)) {
                recordFileData.put(entryName, String.join(",", values));
            }
        }
    }

    private ByteBufferReader getDecompressedByteBufferReader() {
        LZ4Factory factory = LZ4Factory.fastestInstance();
        LZ4SafeDecompressor decompressor = factory.safeDecompressor();

        byte[] decompressed = new byte[recordDataSizeDecompressed];
        byte[] compressed = LERABuffer.get().readBytes(recordDataOffset + ARZDatabase.get().getHeader().getHeaderEndPosition(), recordDataSizeCompressed);
        decompressor.decompress(compressed, 0, recordDataSizeCompressed, decompressed, 0, recordDataSizeDecompressed);

        return new ByteBufferReader(decompressed);
    }

    public void setRecordFile(String recordFile) {
        this.recordFile = recordFile;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("File: " + recordFile);
        sb.append(NL);
        sb.append("Content: [" );
        sb.append(NL);
        for(Map.Entry<String, String> e : recordFileData.entrySet()) {
            sb.append(TAB);
            sb.append(e.getKey());
            sb.append(": ");
            sb.append(e.getValue());
            sb.append(NL);
        }
        sb.append("]" );
        sb.append(NL);
        return sb.toString();
    }
}