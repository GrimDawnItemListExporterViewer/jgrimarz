package knoo.gdie;

/**
 * Created by knoodrake on 21/04/2017.
 */
public class ARZHeader {

    private final static String NL = System.getProperty("line.separator");
    private final static String TAB = "  ";

    public short unknown;
    public short version;
    private int recordTableStart;
    private int recordTableSize;
    private int recordTableEntries;
    private int stringTableStart;
    private int stringTableSize;
    private long headerEndPosition;

    private boolean read = false;

    public ARZHeader() {}

    public void read() {
        if(!read) {
            unknown = LERABuffer.get().readShort();
            version = LERABuffer.get().readShort();
            recordTableStart = LERABuffer.get().readInt();
            recordTableSize = LERABuffer.get().readInt();
            recordTableEntries = LERABuffer.get().readInt();
            stringTableStart = LERABuffer.get().readInt();
            stringTableSize = LERABuffer.get().readInt();
            headerEndPosition = LERABuffer.get().getCurrentPosition();
            read = true;
        }
    }

    public short getUnknown() {
        return unknown;
    }

    public short getVersion() {
        return version;
    }

    public int getRecordTableStart() {
        return recordTableStart;
    }

    public int getRecordTableSize() {
        return recordTableSize;
    }

    public int getRecordTableEntries() {
        return recordTableEntries;
    }

    public int getStringTableStart() {
        return stringTableStart;
    }

    public int getStringTableSize() {
        return stringTableSize;
    }

    @Override
    public String toString() {
        if(!read) {
            return "ARZHeader[not read yet]";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("ARZHeader[ ");
        sb.append(NL);
        sb.append(TAB);
        sb.append("  unknown: ");
        sb.append(unknown);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", version: ");
        sb.append(version);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", recordTableStart: ");
        sb.append(recordTableStart);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", recordTableSize: ");
        sb.append(recordTableSize);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", recordTableEntries: ");
        sb.append(recordTableEntries);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", stringTableStart: ");
        sb.append(stringTableStart);
        sb.append(NL);
        sb.append(TAB);
        sb.append(", stringTableSize: ");
        sb.append(stringTableSize);
        sb.append(NL);
        sb.append("]");
        return sb.toString();
    }

    public long getHeaderEndPosition() {
        return headerEndPosition;
    }
}
