package knoo.gdie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * Created by knoodrake on 26/04/2017.
 */
public class LERABuffer {

    private static LERABuffer __instance;
    private RandomAccessFile randomAccessFile;

    public static LERABuffer get() {
        return __instance;
    }

    public static void init(final String filePath) throws IOException {
        __instance = new LERABuffer(filePath);
    }

    private LERABuffer(final String filePath) throws IOException {
        File file = new File(filePath);
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
        } catch (FileNotFoundException e) {
            randomAccessFile.close();
            e.printStackTrace();
        }
    }

    public long getCurrentPosition() {
        try {
            return randomAccessFile.getFilePointer();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public byte[] readBytes(int length) {
        byte[] bytes = new byte[length];
        getByteBuffer(length).get(bytes);
        return bytes;
    }


    public byte[] readBytes(long offset, int length) {
        byte[] bytes = new byte[length];
        long currentPos = getCurrentPosition();
        try {
            randomAccessFile.seek(offset);
            getByteBuffer(length).get(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                randomAccessFile.seek(currentPos);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bytes;
    }

    public String readString() {
        int strLen = LERABuffer.get().readInt();
        return LERABuffer.get().readString(strLen);
    }

    public String readString(int strLen) {
        byte[] stringByteBuffer = new byte[strLen];
        getByteBuffer(strLen).get(stringByteBuffer);
        StringBuilder sb = new StringBuilder(strLen);
        for(int i=0; i<strLen; i++) {
            sb.append((char)stringByteBuffer[i]);
        }
        return sb.toString();
    }

    public void seek(int position) throws IOException {
        randomAccessFile.seek(position);
    }

    public int readInt() {
        return getByteBuffer(4).getInt();
    }

    public short readShort() {
        return getByteBuffer(2).getShort();
    }

    private ByteBuffer getByteBuffer(int length) {
        byte[] buff = new byte[length];
        try {
            randomAccessFile.readFully(buff);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ByteBuffer.wrap(buff).order(ByteOrder.LITTLE_ENDIAN);
    }

    public void close() throws IOException {
        randomAccessFile.close();
    }
}
