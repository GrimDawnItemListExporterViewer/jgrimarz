package knoo.gdie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by knoodrake on 26/04/2017.
 */
public class ARZDatabase {

    private List<String> keptValues;
    private List<String> recordMustHaveAtLeastOneOf;

    public ARZHeader getHeader() {
        return header;
    }

    private ARZHeader header;
    private int stringCount;
    private List<String> strings;
    private int recordStringCount = 0;

    private static ARZDatabase __instance;

    private Map<String, ARZRecord> records;

    private ARZDatabase() {
        header = new ARZHeader();
        records = new HashMap<>();
    }

    public static ARZDatabase get() {
       if(__instance == null)
           __instance = new ARZDatabase();
        return __instance;
   }

    public void read(Predicate<String> recordNameFilter) throws IOException {
        header.read();
        readStrings();
        readRecords(recordNameFilter);
    }

    private void readRecords(Predicate<String> recordNameFilter) throws IOException {
        LERABuffer.get().seek(header.getRecordTableStart());
        int recordsKept = 0;
        for(int i=0; i<header.getRecordTableEntries(); i++) {
            ARZRecord arzRecord = new ARZRecord();
            arzRecord.read();
            String recordFile = strings.get(arzRecord.recordFileIndex);
            if(recordNameFilter != null && !recordNameFilter.test(recordFile))
                continue;

            arzRecord.decompress(keptValues);
            if(!arzRecord.getRecordFileData().isEmpty()) {
                boolean filtered = !recordMustHaveAtLeastOneOf.isEmpty();
                if(filtered) {
                    for (String key : arzRecord.getRecordFileData().keySet()) {
                        if (recordMustHaveAtLeastOneOf.contains(key)) {
                            filtered = false;
                            break;
                        }
                    }
                }
                if(!filtered) {
                    arzRecord.setRecordFile(recordFile);
                    records.put(strings.get(arzRecord.recordFileIndex), arzRecord);
                    recordsKept++;
                    //System.out.println(arzRecord.toString());
                }
            }
        }
        System.out.println("# of records kept: " + recordsKept);
    }

    private void readStrings() throws IOException {
        LERABuffer.get().seek(header.getStringTableStart());
        stringCount = LERABuffer.get().readInt();
        strings = new ArrayList<>(stringCount);
        for(int i=0; i<stringCount; i++) {
            String string = LERABuffer.get().readString();
            strings.add(string);
            if(string.startsWith("records/")) {
                recordStringCount++;
            }
        }
    }

    public Map<String, ARZRecord> getRecords() {
        return records;
    }

    public int getRecordStringCount() {
        return recordStringCount;
    }

    public List<String> getStrings() {
        return strings;
    }

    public String getString(int index) {
        return strings.get(index);
    }

    public void keepOnlyValues(List<String> keptValues) {
        this.keptValues = keptValues;
    }

    public void recordMustHaveAtLeastOneOf(List<String> recordMustHaveAtLeastOneOf) {
        this.recordMustHaveAtLeastOneOf = recordMustHaveAtLeastOneOf;
    }

}
