package knoo.gdie;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

/**
 * see https://gitlab.com/atom0s/grimarz GPLv3 cpp project
 */
public class ARZDatabaseReader {

    private List<String> keepOnlyValues = new ArrayList<>();
    private List<String> recordMustHaveAtLeastOneOf = new ArrayList<>();
    private Predicate<String> recordNameFilter = null;


    public static void main(String[] args) throws IOException {
        new ARZDatabaseReader().init(args[0]);
    }

    public void init(String pathStr) throws IOException {
        LERABuffer.init(pathStr);
        ARZDatabase.get().keepOnlyValues(keepOnlyValues);
        ARZDatabase.get().recordMustHaveAtLeastOneOf(recordMustHaveAtLeastOneOf);
        try {
            ARZDatabase.get().read(recordNameFilter);
        } finally {
            LERABuffer.get().close();
            ARZDatabase.get().getStrings().clear();
        }

        System.out.println("# of records: " + ARZDatabase.get().getRecordStringCount());
    }

    public void setRecordNameFilter(Predicate<String> filter) { this.recordNameFilter = filter; }

    public List<String> getKeepOnlyValues() {
        return keepOnlyValues;
    }

    public void setKeepOnlyValues(List<String> keepOnlyValues) {
        this.keepOnlyValues = keepOnlyValues;
    }

    public List<String> getRecordMustHaveAtLeastOneOf() {
        return recordMustHaveAtLeastOneOf;
    }

    public void setRecordMustHaveAtLeastOneOf(List<String> recordMustHaveAtLeastOneOf) {
        this.recordMustHaveAtLeastOneOf = recordMustHaveAtLeastOneOf;
    }

    public Predicate<String> getRecordNameFilter() {
        return recordNameFilter;
    }
}
